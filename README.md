# MPDPIPE

Play songs or playlists from youtube with mpd.
Requires mpc and yt-dlp or youtube-dl.

```
USAGE:
mpdpipe URL
mpdpipe MPD_HOST URL
```
